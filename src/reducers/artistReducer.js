import * as types from '../constants/ActionTypes';
import omit from 'lodash/omit';
import assign from 'lodash/assign';
import mapValues from 'lodash/mapValues';
import _ from 'lodash';
import Guid from 'guid';

const initialState = { artists: {}, artist: {}};
/*
const initialState = { artists: {
    'ce8e2937-def8-140c-cfa1-c11ab7249576': {
      Id: 'ce8e2937-def8-140c-cfa1-c11ab7249576',
      Title: 'Muse',
      Alias: 'muse',
      CreatedAt: Date.now()
    },
    'b558088d-03dc-db3b-d78b-912fcce7349b': {
      Id: 'b558088d-03dc-db3b-d78b-912fcce7349b',
      Title: 'Элизиум',
      Alias: 'elizium',
      CreatedAt: Date.now()
    }  
}};
*/
export default function artistReducer(
  state = initialState, action
){
  switch(action.type){
    case types.LOAD_ARTISTS:
      return {
        ...state,
        artists: action.artists
      }
    
    case types.LOAD_ARTIST_BY_ALIAS:
      return {
        ...state,
        artist: action.artist
      }

    case types.ADD_ARTIST:
      return {
        ...state,
        artists: {
          ...state.artists,
          [action.Id]: {
            Id: action.Id,
            Title: action.Title,
            Alias: action.Alias,
            CreatedAt: action.CreatedAt
          }
        }
      }
    
    case types.DELETE_ARTIST:
      return {
        ...state,
        artists: omit(state.artists, action.Id)
      }
        
    default:
      return state;
  }
}