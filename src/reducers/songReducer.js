import * as types from '../constants/ActionTypes';
import omit from 'lodash/omit';
import assign from 'lodash/assign';
import mapValues from 'lodash/mapValues';
import _ from 'lodash';
import Guid from 'guid';

const initialState = { songs: {}};
/*
const initialState = { songs: {
    '4b1c5fc3-40c0-471d-b5ea-ffcd56bbde1c': {
      Id: '4b1c5fc3-40c0-471d-b5ea-ffcd56bbde1c',
      Title: 'The Handler',
      Alias: 'the-hanler',
      Text: 'Text of the handler...',
      ArtistId: 'ce8e2937-def8-140c-cfa1-c11ab7249576',
      CreatedAt: Date.now()      
    },
    '7f26f906-db33-42c9-84de-7d990f27087d': {
      Id: '7f26f906-db33-42c9-84de-7d990f27087d',
      Title: 'New born',
      Alias: 'new-born',
      Text: 'Text of new born...',
      ArtistId: 'ce8e2937-def8-140c-cfa1-c11ab7249576',
      CreatedAt: Date.now()      
    },
    
    'e5960b37-c471-4a12-83b1-2a16ab33a1cd': {
      Id: 'e5960b37-c471-4a12-83b1-2a16ab33a1cd',
      Title: 'Дети-мишени',
      Alias: 'deti-misheni',
      Text: 'Text of the misheni...',
      ArtistId: 'b558088d-03dc-db3b-d78b-912fcce7349b',
      CreatedAt: Date.now()      
    },
    '2e48805f-4ae8-45af-a48d-e57600bac5b8': {
      Id: '2e48805f-4ae8-45af-a48d-e57600bac5b8',
      Title: 'И рассыплется в пыль',
      Alias: 'rassypletsya-v-pyl',
      Text: 'Text of the pyl...',
      ArtistId: 'b558088d-03dc-db3b-d78b-912fcce7349b',
      CreatedAt: Date.now()      
    }  
      
}};
*/
export default function songReducer(
  state = initialState, action
){
  switch(action.type){
    case types.LOAD_SONGS:
      return {
        ...state,
        songs: action.songs
      }
    
    case types.LOAD_SONGS_BY_ARTIST_Id:
      return {
        ...state,
        artistId: action.artistId,
        songs: action.songs
      }

    case types.ADD_SONG:
      return {
        ...state,
        songs: {
          ...state.songs,
          [action.Id]: {
            Id: action.Id,
            Title: action.Title,
            Alias: action.Alias,
            Text: action.Text,
            ArtistId: action.ArtistId,
            CreatedAt: action.CreatedAt
          }
        }
      }
    
    case types.DELETE_SONG:
      return {
        ...state,
        songs: omit(state.songs, action.Id)
      }
        
    default:
      return state;
  }
}