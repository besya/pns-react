import * as types from '../constants/ActionTypes';
import Guid from 'guid';

export function loadArtists(artists) {
  return {
    type: types.LOAD_ARTISTS,
    artists
  }
}

export function loadArtistByAlias(alias, artist) {
  return {
    type: types.LOAD_ARTIST_BY_ALIAS,
    alias,
    artist
  }
}

export function addArtist(Title, Alias) {
  const Id = Guid.raw();
  const CreatedAt = Date.now();
  return {
    type: types.ADD_ARTIST,
    Id,
    Title,
    Alias,
    CreatedAt
  };
}

export function deleteArtist(Id){
  return {
    type: types.DELETE_ARTIST,
    Id
  }
}

export function loadSongs(songs) {
  return {
    type: types.LOAD_SONGS,
    songs
  }
}

export function loadSongsByArtistId(artistId, songsByArtistId) {
  return {
    type: types.LOAD_SONGS_BY_ARTIST_ID,
    artistId,
    songsByArtistId
  }
}

export function addSong(ArtistId, Title, Alias, Text) {
  const Id = Guid.raw();
  const CreatedAt = Date.now();
  return {
    type: types.ADD_SONG,
    Id,
    ArtistId,
    Title,
    Alias,
    Text,
    CreatedAt
  };
}

export function deleteSong(Id){
  return {
    type: types.DELETE_SONG,
    Id
  }
}