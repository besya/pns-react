import React, { Component, PropTypes } from 'react';
import {Link} from 'react-router';

export default class ArtistsListItem extends Component{
  static propTypes = {
    Id: PropTypes.string.isRequired,
    Title: PropTypes.string.isRequired,
    Alias: PropTypes.string.isRequired,
  };
  
  render(){
    const { Id, Title, Alias } = this.props; 
    
    return (
    <Link className="list-group-item" to={`/${Alias}`}>{Title}</Link>
    );
  }
}