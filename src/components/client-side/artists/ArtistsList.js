import React, { Component, PropTypes } from 'react';
import ArtistListItem from './ArtistListItem';
import _ from 'lodash';

export default class ArtistsList extends Component{
  static propTypes = {
    artists: PropTypes.array.isRequired,
    loadArtists: PropTypes.func.isRequired
  };
  
  componentDidMount(){
    this.props.loadArtists();
  }
  
  render(){
    const { artists } = this.props;
    
    let artistItem = (item) => {
      return <ArtistListItem key={item.Id} Id={item.Id} Title={item.Title} Alias={item.Alias} />;
    };
    
    let artistsList = _.values(artists).map(artistItem);
    
    return (
      <div className="list-group">{artistsList}</div>
    );
  }
}