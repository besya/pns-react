import React, { Component, PropTypes } from 'react';

export default class ArtistItem extends Component{
  static propTypes = {
    //Id: PropTypes.string.isRequired,
    //Title: PropTypes.string.isRequired,
    artist: PropTypes.object.isRequired,
    Alias: PropTypes.string.isRequired,
    loadArtistByAlias: PropTypes.func.isRequired
  };
  
  componentDidMount(){
    this.props.loadArtistByAlias(this.props.Alias);
  }
  
  render(){
    return (
      <div>
        <h1>{this.props.artist.Title}</h1>
      </div>
    );
  }
}