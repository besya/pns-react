import React, { Component } from 'react';
import { Link } from 'react-router';

export default class Header extends Component {
  render(){
    return (
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="navbar-header">
            <Link to="/" className="navbar-brand">Brand</Link>
          </div>
          <ul className="nav navbar-nav">
            <li><Link to="/artists">Artists</Link></li>
            <li><Link to="/songs">Songs</Link></li>
          </ul>
        </div>
      </nav>
    );
  }
}