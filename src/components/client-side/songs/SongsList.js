import React, { Component, PropTypes } from 'react';
import SongsListItem from './SongsListItem';
import _ from 'lodash';

export default class SongsList extends Component{
  static propTypes = {
    artists: PropTypes.array.isRequired, 
    songs: PropTypes.array.isRequired,
    loadArtists: PropTypes.func.isRequired,
    loadSongs: PropTypes.func.isRequired
  };

  componentDidMount(){
    this.props.loadArtists();
    this.props.loadSongs();
  }
  
  render(){
    const { artists, songs } = this.props;
    
    let songItem = (item) => {
      let artist = _.find(artists, {'Id': item.ArtistId});
      return <SongsListItem key={item.Id} Id={item.Id} Title={item.Title} Alias={item.Alias} Text={item.Text} artist={artist} />;
    };
    
    let songsList = songs.map(songItem);
    
    return (
      <div className="list-group">{songsList}</div>
    );
  }
}