import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

export default class SongItem extends Component{
  static propTypes = {
    Id: PropTypes.string.isRequired,
    Title: PropTypes.string.isRequired,
    Alias: PropTypes.string.isRequired,
    Text: PropTypes.string.isRequired,
    artist: PropTypes.object.isRequired,
  };
  
  render(){
    const { Id, Title, Alias, Text, artist } = this.props; 
    
    return (
      <div>
        <h1><Link to={`/${artist.Alias}`}>{artist.Title}</Link> - {Title}</h1>
        <div>{Text}</div>
      </div>
    );
  }
}