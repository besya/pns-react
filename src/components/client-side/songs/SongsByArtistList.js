import React, { Component, PropTypes } from 'react';
import SongsListItem from './SongsListItem';
import _ from 'lodash';

export default class SongsByArtistList extends Component{
  static propTypes = {
    artist: PropTypes.object.isRequired,
    songs: PropTypes.array.isRequired,
    loadSongsByArtistId: PropTypes.func.isRequired,
    loadArtistByAlias: PropTypes.func.isRequired,
    Alias: PropTypes.string.isRequired,
  };

  componentDidMount(){
    this.props.loadSongsByArtistId(this.props.artist.Id);
  }
  
  render(){
    const { artist, songs } = this.props;
    
    let songItem = (item) => {
      return <SongsListItem key={item.Id} Id={item.Id} Title={item.Title} Alias={item.Alias} Text={item.Text} artist={artist} />;
    };
    
    let songsList = songs.map(songItem);
    
    return (
      <div className="list-group">{songsList}</div>
    );
  }
}