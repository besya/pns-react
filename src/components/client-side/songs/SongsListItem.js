import React, { Component, PropTypes } from 'react';
import {Link} from 'react-router';

export default class SongsListItem extends Component{
  static propTypes = {
    Id: PropTypes.string.isRequired,
    Title: PropTypes.string.isRequired,
    Alias: PropTypes.string.isRequired,
    Text: PropTypes.string.isRequired,
    artist: PropTypes.object.isRequired
  };
  
  render(){
    const { Id, Title, Alias, Text, artist } = this.props; 
    
    return (
      <Link className="list-group-item" to={`/${artist.Alias}/${Alias}`}>{Title} <span className="pull-right">{artist.Title}</span></Link>
    );
  }
}