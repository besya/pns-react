import React, { Component, PropTypes} from 'react';
import ArtistListItem from './ArtistListItem';
import _ from 'lodash';
import styles from './ArtistList.scss';

export default class ArtistList extends Component {
  static propTypes = {
    artists: PropTypes.object.isRequired,
    deleteArtist: PropTypes.func.isRequired,
    loadArtists: PropTypes.func.isRequired
  }

  componentDidMount(){
    this.props.loadArtists();
  }

  render(){
    const {artists, deleteArtist} = this.props;
    
    let artistItem = (item) => {
      return <ArtistListItem key={item.Id} Id={item.Id} Title={item.Title} Alias={item.Alias} CreatedAt={item.CreatedAt} deleteArtist={deleteArtist} />;
    };

    let artistList = _.sortBy(_.values(artists), ['CreatedAt']).reverse().map(artistItem);
    
    return (
      <table className="table table-striped">
        <thead><tr><th>Id</th><th>Title</th><th>Alias</th><th>CreatedAt</th><th>Actions</th></tr></thead>
        <tbody>
          {artistList}
        </tbody>
      </table>
    );
  }
}
