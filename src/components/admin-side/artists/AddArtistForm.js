import React, {Component, PropTypes} from 'react';

export default class AddArtistForm extends Component {
  static propTypes = {
    addArtist: PropTypes.func.isRequired
  }
  
  constructor (props, context) {
    super(props, context);
    this.state = {
      Title: this.props.Title || '',
      Alias: this.props.Alias || ''
    };
  }
  
  handleSubmit (e) {
    e.preventDefault();
    const Title = this.state.Title;
    const Alias = this.state.Alias || '123';

    if(Title != '' && Alias != ''){
      this.props.addArtist(Title, Alias);
      this.setState({ Title: '', Alias: '' });
    }
  }
  
  handleChangeTitle (e) {
    this.setState({ ...this.state, Title: e.target.value });
  }

  handleChangeAlias (e) {
    this.setState({ ...this.state, Alias: e.target.value });
  }
  
  render() {
    return (
      <form className="form-inline">
        <div className="form-group">
          <label className="sr-only" for="inputArtistTitle">Artist Title</label>
          <input value={this.state.Title} onChange={this.handleChangeTitle.bind(this)} type="text" className="form-control" id="inputArtistTitle" placeholder="Artist Title" />
        </div>
        <div className="form-group">
          <label className="sr-only" for="inputArtistAlias">Artist Alias</label>
          <input value={this.state.Alias} onChange={this.handleChangeAlias.bind(this)} type="text" className="form-control" id="inputArtistAlias" placeholder="Artist Alias" />
        </div>
        <button onClick={this.handleSubmit.bind(this)} className="btn btn-default">Add</button>
      </form>      
    );
  }
}