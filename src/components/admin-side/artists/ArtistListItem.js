import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import styles from './ArtistListItem.scss';
import DateTime from 'node-datetime';
import { Link } from 'react-router'

export default class ArtistListItem extends Component {
  static propTypes = {
    Id: PropTypes.string.isRequired,
    Title: PropTypes.string.isRequired,
    Alias: PropTypes.string.isRequired,
    CreatedAt: PropTypes.number.isRequired,
    deleteArtist: PropTypes.func.isRequired
  }
  
  render () {
    return(
      <tr>
        <td>{this.props.Id}</td>
        <td><Link to={`/admin/artists/${this.props.Alias}`}>{this.props.Title}</Link></td>
        <td>{this.props.Alias}</td>
        <td>{DateTime.create(this.props.CreatedAt).format('m/d/Y H:M:S')}</td>
        <td>
          <button className={`btn btn-default ${styles.btnAction}`} onClick={() => this.props.deleteArtist(this.props.Id)}>
            <i className="fa fa-trash" />
          </button>
        </td>
      </tr>
    );
  }
}