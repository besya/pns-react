import React, {Component, PropTypes} from 'react';

export default class AddSongForm extends Component {
  static propTypes = {
    artists: PropTypes.object.isRequired,
    addSong: PropTypes.func.isRequired,
    selectedArtist: PropTypes.object.isRequired
  }
  
  constructor (props, context) {
    super(props, context);
    this.state = {
      Title: this.props.Title || '',
      Alias: this.props.Alias || '',
      ArtistId: this.props.selectedArtist.Id || '',
      Text: this.props.Text || ''
    };
  }
  
  handleSubmit (e) {
    e.preventDefault();
    const Title = this.state.Title;
    const Alias = this.state.Alias || '123';
    const Text = this.state.Text;
    const ArtistId = this.state.ArtistId;

    if(Title != '' && Alias != '' && ArtistId != '' && Text != ''){
      this.props.addSong(ArtistId, Title, Alias, Text);
      this.setState({ Title: '', Alias: '', Text: '', ArtistId: '' });
    }
  }
  
  handleChangeTitle (e) {
    this.setState({ ...this.state, Title: e.target.value });
  }

  handleChangeAlias (e) {
    this.setState({ ...this.state, Alias: e.target.value });
  }

  handleChangeText (e) {
    this.setState({ ...this.state, Text: e.target.value });
  }

  handleChangeArtistId (e) {
    this.setState({ ...this.state, ArtistId: e.target.value });
  }
  
  render() {
    
    const {artists} = this.props;
    
    let artistItem = (item) => {
      return <option key={item.Id} value={item.Id}>{item.Title}</option>
    };
    
    let artistList = _.values(artists).map(artistItem);
    
    return (
      <form className="form-horizontal">
        <div className="form-group">
          <label for="inputSongTitle" className="col-sm-2 control-label">Song Title</label>
          <div className="col-sm-10">
            <input value={this.state.Title} onChange={this.handleChangeTitle.bind(this)} type="text" className="form-control" id="inputSongTitle" placeholder="Song Title" />
          </div>
        </div>
        <div className="form-group">
          <label for="inputSongAlias" className="col-sm-2 control-label">Song Alias</label>
          <div className="col-sm-10">
            <input value={this.state.Alias} onChange={this.handleChangeAlias.bind(this)} type="text" className="form-control" id="inputSongAlias" placeholder="Song Alias" />
          </div>
        </div>
        <div className="form-group">
          <label for="inputSongText" className="col-sm-2 control-label">Song Text</label>
          <div className="col-sm-10">
            <input value={this.state.Text} onChange={this.handleChangeText.bind(this)} type="text" className="form-control" id="inputSongText" placeholder="Song Text" />
          </div>
        </div>
        <div className="form-group">
          <label for="inputSongArtist" className="col-sm-2 control-label">Artist</label>
          <div className="col-sm-10">
            <select className="form-control" id="inputSongArtist" defaultValue={this.props.selectedArtist.Id} onChange={this.handleChangeArtistId.bind(this)}>{artistList}</select>
          </div>
        </div>
        <div className="form-group">
          <div className="col-sm-offset-2 col-sm-10">
            <button onClick={this.handleSubmit.bind(this)} type="submit" className="btn btn-default">Add</button>
          </div>
        </div>
      </form>      
    );
  }
}