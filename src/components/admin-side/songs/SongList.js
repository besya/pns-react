import React, { Component, PropTypes} from 'react';
import SongListItem from './SongListItem';
import _ from 'lodash';
import styles from './SongList.scss';

export default class SongList extends Component {
  static propTypes = {
    artist: PropTypes.object,
    songs: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
  }

  render(){
    const {songs, actions} = this.props;
    
    let songItem = (item) => {
      return <SongListItem key={item.Id} Id={item.Id} Title={item.Title} Alias={item.Alias} Text={item.Text} CreatedAt={item.CreatedAt} {...actions} />;
    };

    let songList = _.sortBy(songs, ['CreatedAt']).reverse().map(songItem);
    
    return <ul className={styles.songList}>{songList}</ul>;
  }
}
