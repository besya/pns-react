import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import styles from './SongListItem.scss';
import DateTime from 'node-datetime';
import { Link } from 'react-router'

export default class SongListItem extends Component {
  static propTypes = {
    Id: PropTypes.string.isRequired,
    Title: PropTypes.string.isRequired,
    Alias: PropTypes.string.isRequired,
    Text: PropTypes.string.isRequired,
    CreatedAt: PropTypes.number.isRequired,
    deleteSong: PropTypes.func.isRequired,
  }
  
  render () {
    return(
      <li className={styles.songListItem}>
        <div className={styles.songInfos}>
          <div><span>{this.props.Title} - {DateTime.create(this.props.CreatedAt).format('m/d/Y H:M:S')}</span></div>
          <div><small>{this.props.Alias}</small></div>
          <div><small>{this.props.Text}</small></div>
        </div>
        <div className={styles.songActions}>
          <button className={`btn btn-default ${styles.btnAction}`} onClick={() => this.props.deleteSong(this.props.Id)}>
            <i className="fa fa-trash" />
          </button>
        </div>
      </li>
    );
  }
}