import Firebase from 'firebase';
import Fireproof from 'fireproof';
import * as types from '../constants/ActionTypes';
import Guid from 'guid';
import _ from 'lodash';

const fireRef = new Firebase('https://playnsing.firebaseio.com');
const fp = new Fireproof(fireRef);
const artists = fp.child('artists');
const songs = fp.child('songs');

const firebaseMiddleware = store => next => {
  return action => {
    switch (action.type) {
      case types.LOAD_ARTISTS:
        return artists.orderByChild('CreatedAt').then(snap => {
          return next({...action, artists: snap.val()});
        });
      
      case types.LOAD_ARTIST_BY_ALIAS:
        return artists.orderByChild('Alias').equalTo(action.alias).then(snap => {
          return next({...action, artist: {..._.values(snap.val())[0]} });
        });

      case types.ADD_ARTIST:
        const newArtist = {Id: action.Id, Title: action.Title, Alias: action.Alias, CreatedAt: action.CreatedAt};
        return artists.child(action.Id).set(newArtist).then(
          () => { return next({...action, ...newArtist}) }
        );
        
      case types.DELETE_ARTIST:
        return artists.child(action.Id).remove().then(
          () => { return next({...action}) }
        );
     
      case types.LOAD_SONGS:
        return songs.orderByChild('CreatedAt').then(snap => {
          return next({...action, songs: snap.val()});
        });
      
      case types.LOAD_SONGS_BY_ARTIST_ID:
        console.log(action.artistId);
        return songs.orderByChild('ArtistId').equalTo(action.artistId).then(snap => {
          return next({...action, songs: snap.val() });
        });

      case types.ADD_SONG:
        const newSong = {Id: action.Id, Title: action.Title, Alias: action.Alias, Text: action.Text, ArtistId: action.ArtistId, CreatedAt: action.CreatedAt};
        return songs.child(action.Id).set(newSong).then(
          () => { return next({...action, ...newSong}) }
        );
        
      case types.DELETE_SONG:
        return songs.child(action.Id).remove().then(
          () => { return next({...action}) }
        );
     
      default:
        const nextState = next(action);
        return nextState;                 
    }    
  }
};

export default firebaseMiddleware;