import React from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
//import { reduxReactFirebase, firebaseStateReducer } from 'redux-react-firebase'
// Exported from redux-devtools
import { createDevTools, persistState  } from 'redux-devtools';
import rootReducer from '../reducers';
// Monitors are separate packages, and you can make a custom one
import LogMonitor from 'redux-devtools-log-monitor';
import DockMonitor from 'redux-devtools-dock-monitor';
import promiseMiddleware from './promiseMiddleware';
import firebaseMiddleware from './firebaseMiddleware';

export let configureStore = createStore;

/*
export function DevTools(store) {
  if (__DEV__) {
    return (
      <DebugPanel top right bottom>
        <DevTools store={store} monitor={LogMonitor} />
      </DebugPanel>
    );
  }
  return null;
}
*/
export let DevTools = null;
if (__DEV__) {
  DevTools = createDevTools(
      // Monitors are individually adjustable with props.
      // Consult their repositories to learn about those props.
      // Here, we put LogMonitor inside a DockMonitor.
      // Note: DockMonitor is visible by default.
      <DockMonitor toggleVisibilityKey='ctrl-h'
                    changePositionKey='ctrl-q'
                    defaultIsVisible={true}>
        <LogMonitor theme='tomorrow' />
      </DockMonitor>
  );  
  function getDebugSessionKey() {
    // You can write custom logic here!
    // By default we try to read the key from ?debug_session=<key> in the address bar
    const matches = window.location.href.match(/[?&]debug_session=([^&#]+)\b/);
    return (matches && matches.length > 0)? matches[1] : null;
  }

  const enhancer = compose(
    // Middleware you want to use in development:
    applyMiddleware(firebaseMiddleware),
    //applyMiddleware( {firebase: firebaseStateReducer}),
    // Required! Enable Redux DevTools with the monitors you chose
    DevTools.instrument(),
    // Optional. Lets you write ?debug_session=<key> in address bar to persist debug sessions
    persistState(getDebugSessionKey())
  );
  
  configureStore = function(initialState) {
    // Note: only Redux >= 3.1.0 supports passing enhancer as third argument.
    // See https://github.com/rackt/redux/releases/tag/v3.1.0
    const store = createStore(initialState, enhancer);

    // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
    if (module.hot) {
      module.hot.accept('../reducers', () =>
        store.replaceReducer(require('../reducers')/*.default if you use Babel 6+ */)
      );
    }

    return store;
  }
}

// createDevTools takes a monitor and produces a DevTools component


//if (__DEV__) {
/*
  createStore = compose(
    DevTools.instrument(),
    persistState(
      getDebugSessionKey()
    ),
    createStore
  );*/
//}

