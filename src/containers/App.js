import React, { Component } from 'react';
import { combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';

import { configureStore, DevTools } from '../utils/devTools';

import { AdminApp, AdminArtistApp, AdminSongApp } from './admin-side'; 
import { ClientApp, ClientSongs, ClientSong, ClientArtists, ClientArtist } from './client-side';

import * as reducers from '../reducers';

const reducer = combineReducers({...reducers, routing: routerReducer});
const store = configureStore(reducer);

import * as actions from '../actions/Actions';

//store.dispatch(actions.loadArtists());
//store.dispatch(actions.loadSongs());

const history = syncHistoryWithStore(browserHistory, store)

export default class App extends Component {
  render() {
    return (
      <div>
        <Provider store={store}>
          <Router history={history}>
            <Route path="/admin" component={AdminApp}>
              <Route path="artists" component={AdminArtistApp} /> 
              <Route path="artists/:artistAlias" component={AdminSongApp} /> 
            </Route>
            <Route path="/" component={ClientApp}>
              <Route path="songs" component={ClientSongs} />
              <Route path="artists" component={ClientArtists} />
              <Route path=":artistAlias" component={ClientArtist} />
              <Route path=":artistAlias/:songAlias" component={ClientSong} />
            </Route>            
          </Router>        
        </Provider>
        <DevTools store={store}/>
      </div>
    );
  }
}
