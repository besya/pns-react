import React, {Component, PropTypes} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/Actions';
import { AdminArtistList, AdminAddArtistForm } from '../../components';

@connect(state => ({ artistReducer: state.artistReducer}))
export default class ArtistListApp extends Component {
  static propTypes = {
    artistReducer: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  }
  
  render(){
    const { artistReducer: {artists}, dispatch} = this.props;
    const actions = bindActionCreators(Actions, dispatch);
    
    return (
      <div className="container">
        <h1>Artists</h1>
        <div>
          <h3>Add New Artist</h3>
          <AdminAddArtistForm addArtist={actions.addArtist}/>
        </div>
        <div>
          <h3>Artists List</h3>
          <AdminArtistList artists={artists} deleteArtist={actions.deleteArtist} loadArtists={actions.loadArtists} />
        </div>
      </div>
    );
  }
}