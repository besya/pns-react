import React, {Component, PropTypes} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/Actions';
import { AdminSongList, AdminAddSongForm } from '../../components';

@connect(state => ({ artistReducer: state.artistReducer, songReducer: state.songReducer}))
export default class SongListApp extends Component {
  static propTypes = {
    artistReducer: PropTypes.object.isRequired,
    songReducer: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  }
  
  constructor(props, context){
    super(props, context);
    this.state = {
      selectedArtist: null
    }
  }
  
  componentWillMount(){
    const alias = this.props.params.artistAlias;
    if(alias == '') return; 
    
    const {artistReducer: {artists}} = this.props;
    const selectedArtist = _.find(_.values(artists), {'Alias': alias});
    
    this.setState({
      selectedArtist
    });
    
  }
  
  render(){
    const { params: {artistAlias}, artistReducer: {artists}, songReducer: {songs}, dispatch} = this.props;
    const actions = bindActionCreators(Actions, dispatch);
    
    let songList = _.values(songs);
    
    if(this.state.selectedArtist != null) {
      songList = _.filter(_.values(songs), {ArtistId: this.state.selectedArtist.Id});
    }
    
    return (
      <div className="container">
        <h1>{this.state.selectedArtist.Title} Songs</h1>
        <div>
          <h3>Add New Song</h3>
          <AdminAddSongForm selectedArtist={this.state.selectedArtist} artists={artists} addSong={actions.addSong}/>
        </div>
        <div>
          <h3>Songs List</h3>
          <AdminSongList artistAlias={artistAlias} songs={songList} actions={actions} />
        </div>
      </div>      
    );
  }
}