import React, {Component, PropTypes} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/Actions';
import { AdminHeader } from '../../components';

export default class AdminApp extends Component {
  
  render(){    
    return (
      <div>
        <AdminHeader />
        {this.props.children}
      </div>
    );
  }
}