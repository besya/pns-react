import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ClientSongsByArtistList, ClientArtistItem } from '../../components';
import * as Actions from '../../actions/Actions';

@connect(state => ({ artistReducer: state.artistReducer, songReducer: state.songReducer}))
export default class Artist extends Component {
  static propTypes = {
    artistReducer: PropTypes.object.isRequired, 
    songReducer: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  }
    
  render(){
    const { params: {artistAlias}, artistReducer: {artists, artist}, songReducer: {songs}, dispatch } = this.props;
    const actions = bindActionCreators(Actions, dispatch);

    //const artist = _.find(_.values(artists), {'Alias': artistAlias});
    //let songsArray = _.filter(_.values(songs), {ArtistId: artist.Id});
    //<ClientSongsList artists={artists} songs={_.keyBy(songsArray, 'Id')}/>
    //<ClientSongsByArtistList artistAlias={artistAlias} songs={_.values(songs)} loadSongsByArtistId={actions.loadSongsByArtistId} />
    return (
      <div className="container">
        <ClientArtistItem artist={artist} Alias={artistAlias} loadArtistByAlias={actions.loadArtistByAlias} />
        <ClientSongsByArtistList artist={artist} Alias={artistAlias} loadArtistByAlias={actions.loadArtistByAlias} songs={_.values(songs)} loadSongsByArtistId={actions.loadSongsByArtistId} />
      </div>
    );
  }
}