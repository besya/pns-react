import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ClientSongItem } from '../../components';

@connect(state => ({ artistReducer: state.artistReducer, songReducer: state.songReducer}))
export default class Song extends Component {
  static propTypes = {
    artistReducer: PropTypes.object.isRequired,
    songReducer: PropTypes.object.isRequired
  }
  
  constructor(props, context){
    super(props, context);
    this.state = {
      artist: null,
      song: null
    };
  }
  
  componentWillMount(){
    const {artistReducer: {artists}, songReducer: {songs}, params: {artistAlias, songAlias}} = this.props;
    if(artistAlias == '' || songAlias == '') return;
    const artist = _.find(_.values(artists), {'Alias': artistAlias});
    const song = _.find(_.values(songs), {'Alias': songAlias});
    this.setState({artist, song});
  }
  
  render(){
    const { song: {Id, Title, Alias, Text}, artist } = this.state;
    return (
      <div className="container">
        <ClientSongItem Id={Id} Title={Title} Alias={Alias} Text={Text} artist={artist}/>
      </div>
    );
  }
}