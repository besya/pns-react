import React, {Component, PropTypes} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as Actions from '../../actions/Actions';
import { ClientHeader } from '../../components';

export default class ClientApp extends Component {
  
  render(){    
    return (
      <div>
        <ClientHeader />
        {this.props.children}
      </div>
    );
  }
}