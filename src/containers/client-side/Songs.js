import React, { Component, PropTypes } from 'react';
import { ClientSongsList } from '../../components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions/Actions';
import _ from 'lodash';

@connect(state => ({ artistReducer: state.artistReducer, songReducer: state.songReducer}))
export default class Songs extends Component {
  static propTypes = {
    artistReducer: PropTypes.object.isRequired,
    songReducer: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  }
  
  render(){
    const { artistReducer: {artists}, songReducer: {songs}, dispatch } = this.props;
    const actions = bindActionCreators(Actions, dispatch);
    
    return (
      <div className="container">
        <h1>Songs</h1>
        <ClientSongsList artists={_.values(artists)} songs={_.values(songs)} loadArtists={actions.loadArtists} loadSongs={actions.loadSongs}/>
      </div> 
    );
  }
}