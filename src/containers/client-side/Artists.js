import React, { Component, PropTypes } from 'react';
import { ClientArtistsList } from '../../components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../../actions/Actions';
import _ from 'lodash';

@connect(state => ({ artistReducer: state.artistReducer}))
export default class Artists extends Component {
  static propTypes = {
    artistReducer: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  }
  
  render(){
    const { artistReducer: {artists}, dispatch} = this.props;
    const actions = bindActionCreators(Actions, dispatch);
    
    return (
      <div className="container">
        <h1>Artists</h1>
        <ClientArtistsList artists={_.values(artists)} loadArtists={actions.loadArtists}/>
      </div>
    );
  }
}